package com.company;

abstract class EngineFunctionality {
    void startEngine(){
        System.out.println("Engine is running..");
    }
    void shutOffEngine(){
        System.out.println("Engine is switched off");
    }
}
