package com.company;

abstract class TransmissionFunctionality extends EngineFunctionality {
    void speedUp(){
        System.out.println("Speed is increased..");
    }
    void speedDown(){
        System.out.println("Speed is reduced.. ");
    }
}
